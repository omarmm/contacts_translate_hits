<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportedContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imported_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('hits');	
            $table->timestamps();
            $table->Index('name');	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( "imported_contacts", function( $table )
        {
            $table->dropIndex(['name']);
            $table->dropColumn('name');
        } );
        Schema::dropIfExists('imported_contacts');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslatedContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translated_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('imported_contacts_id');
            $table->string('language_code');
            $table->timestamps();
            $table->foreign('imported_contacts_id')->references('id')->on('imported_contacts');
            $table->Index('name');	
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      
        Schema::table( "translated_contacts", function( $table )
        {
            $table->dropForeign(['imported_contacts_id']);
            $table->dropColumn('imported_contacts_id');
            $table->dropIndex(['name']);
            $table->dropColumn('name');
	
        } );
        Schema::dropIfExists('translated_contacts');

        

    }
}

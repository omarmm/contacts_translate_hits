<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
              <!-- =============== Custom Content ===========-==========-->
               {{-- Start alert messages --}}
              <div class="col-lg-12 text-center">
                @if (Session::has('success'))
                  <div class="alert text-center" style="background-color: #2ecc71; color: white; padding: 10px; margin-top: 20px;">
                    <b>{{ Session::get('success') }}</b>
                  </div>
                @endif

                @if (Session::has('warning'))
                  <div class="alert text-center" style="background-color: #f39c12; color: white; padding: 10px; margin-top: 20px;">
                    <b>{{ Session::get('warning') }}</b>
                  </div>
                @endif

                @if (Session::has('error'))
                  <div class="alert text-center" style="background-color: #c0392b; color: white; padding: 10px; margin-top: 20px;">
                    <b>{{ Session::get('error') }}</b>
                  </div>
                @endif

                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger text-center" style="background-color: #c0392b; color: white; padding: 10px; margin-top: 20px;">
                      <b>{{ $error }}</b>
                    </div>
                @endforeach

              </div>
              {{-- End alert --}}

<div class="container mt-3">
  <h2>Upload JSON File</h2>
  <p>Please choose JSON file from your PC , Then click submit:</p>
  <form action="{{ route('upload') }}" method="post" enctype="multipart/form-data" accept-charset="utf-8">
  {{ csrf_field() }}

    <p>JSON file:</p>
    <div class="custom-file mb-3">
      <input type="file" class="custom-file-input" id="customFile" name="filename">
      <label class="custom-file-label" for="customFile">Choose file</label>
    </div>
    
    <div class="mt-3">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>

<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>

</body>
</html>

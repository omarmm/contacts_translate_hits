
<body lang=EN-US link=blue vlink="#954F72">

<div class=WordSection1>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>1-clone the project</p>

<p class=MsoNormal>2-run : <span style='background:silver'>composer install</span></p>

<p class=MsoNormal>3-create a new database on mysql</p>

<p class=MsoNormal>4-set database variables in .env file </p>

<p class=MsoNormal>5-<span style='color:red'>Important! , </span><span
style='color:#BFBFBF;background:black'>add GOOGLE_TRANSLATE_API_KEY=
YOUR_API_KEY to .env file .. replace your api_key with 'YOUR_API_KEY'</span></p>

<p class=MsoNormal>6-run : <span style='color:black;background:silver'>php
artisan migrate</span></p>

<p class=MsoNormal>7- run : <span style='background:silver'>php artisan serve</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'>8- run : <span style='font-size:9.0pt;font-family:
"Lucida Console";background:silver'>php artisan queue:listen</span><span
style='font-size:9.0pt;font-family:"Lucida Console"'> and keep it running.</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span style='font-size:9.0pt;font-family:"Lucida Console"'>&nbsp;</span></p>

<p class=MsoNormal>9  when you access the <a href="http://localhost:8000/">http://localhost:8000/</a>
, you will find a simple (json upload) form</p>

<p class=MsoNormal>10- select JSON file with the same format like in the
sample.json file</p>

<p class=MsoNormal>11 submit </p>

<p class=MsoNormal>12- please check the data base , you will notice 2 main
tables imported_contacts and translated_contacts.</p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in'>-<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span dir=LTR></span><b>Imported_contacts:</b> contains typical contacts
info imported from sample.json file.</p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'>-<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span dir=LTR></span><b>Translated_contacts: </b>contains translated contacts
(if original contacts is in English then you will find the Arabic translated
text under <b>name</b> column and vice versa )</p>

<p class=MsoListParagraphCxSpLast style='text-indent:-.25in'>-<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span dir=LTR></span><b>Both of imported_contacts and translated_contacts
connected to each other by the foreign key (imported_contacts_id) in translated_contacts
table . </b></p>

<p class=MsoNormal><b>&nbsp;</b></p>

</div>

</body>

</html>

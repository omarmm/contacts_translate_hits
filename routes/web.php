<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/translate_test', function () {
  //  $text= 'قط';
      $text= 'cat';
   $detect= GoogleTranslate::detectLanguage( $text);
   //dd($detect['language_code']);
   // $translation= GoogleTranslate::justTranslate($text,'ar');
   $translation= GoogleTranslate::unlessLanguageIs('ar',$text,'ar');

    dd($translation);

});

Route::get('/', 'ContactsController@index')->name('home');
Route::post('/upload', 'ContactsController@upload')->name('upload');

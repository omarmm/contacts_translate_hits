<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TranslatedContacts extends Model
{
    protected $fillable = ['name','imported_contacts_id','language_code'];
}

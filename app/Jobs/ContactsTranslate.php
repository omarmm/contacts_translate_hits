<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\ImportedContacts;
use App\TranslatedContacts;
use Session;

class ContactsTranslate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $json = null;
    public function __construct($json)
    {
        $this->json = $json;
    }



    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    { 
        $names_array =[];
        $hits_array = [];

        foreach($this->json as $key => $data) {
            foreach ($data as $data_key => $data_string) {
                $data_array= json_decode($data_string, true);
                if($data_key=='names'){
                    foreach ($data_array as $key_array => $value) {
                        array_push($names_array,  $value);                 
                    }
                }else{
                    foreach ($data_array as $key_array => $value) {
                        array_push($hits_array,  $value);                 
                    }                 
                }
            }
        }
       // dd($hits_array);
          $contacts_array = array();
          foreach ($names_array as $key => $name) {
            $hits = $hits_array[$key];
            $contacts_array[$key] = ['name'=>$name,'hits'=>$hits];
          }

          foreach ($contacts_array as  $contacts) {
           $contacts['name']= $contacts['name']==null?'null':$contacts['name'];
           $imported= ImportedContacts::create([
                'name'=>$contacts['name'],   
                'hits'=>$contacts['hits'],
                ]);
    
                /*here we will translate
                1- check first if name count >1 then dont translate and hits++
                2- detect language 
                3- if arabic translate to english else translate to arabic
                */
                $text= $imported->name;
               $contacts_count =  ImportedContacts::where('name',$text)->count();

               if($contacts_count<=1){
                $detect= \GoogleTranslate::detectLanguage( $text);
                $lang= $detect['language_code']=='en'?'ar':'en';
                $translation= \GoogleTranslate::justTranslate($text,$lang);
                
                
                //insert trnslation

                $translate= TranslatedContacts::create([
                    'name'=>$translation,   
                    'imported_contacts_id'=>$imported->id,
                    'language_code'=>$lang
                    ]);

                // increase hits by one
                $imported->hits= $imported->hits+1;

               }else{
                  //if duplicated        
                  // increase hits by one
                $imported->hits= $imported->hits+1;

               }
               $imported->save();              

          }
    }
}

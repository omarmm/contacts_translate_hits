<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\ImportedContacts;
use App\TranslatedContacts;
use Session;
use App\Jobs\ContactsTranslate;




class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('contacts.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {

        if(isset($request['filename']))
        {
        $filename = Storage::putFile('json_files', $request['filename']);

        $path = storage_path('app/'.$filename);
  
        $json = json_decode(file_get_contents($path), true); 

        $ContactsTranslateJob = new ContactsTranslate($json);
        $this->dispatch($ContactsTranslateJob);

        }else{ //no imported file
        Session::flash('error', 'No file has been chosen!');
        return redirect('/');
        }
        Session::flash('success', 'Contacts imported Successfully!');
        return redirect('/');
    }

  
}

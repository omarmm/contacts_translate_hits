<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportedContacts extends Model
{
    //
    protected $fillable = ['name','hits'];

}
